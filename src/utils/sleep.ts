/**
 * функция для задержки
 * @param time время задержки в мс
 */
export const sleep = async (time: number) =>
  await new Promise(resolve => {
    setTimeout(() => resolve(true), time);
  });
