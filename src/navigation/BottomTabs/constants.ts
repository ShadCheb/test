import {Dimensions, PixelRatio} from 'react-native';

// TODO: Доделать нижний таббар
// const {width} = Dimensions.get('window');

export interface IconProps {
  active?: boolean;
}

// const numberOfIcons = 3;
// const horizontalPading = 80;

// export const SEGMENT =
//   PixelRatio.roundToNearestPixel(width / numberOfIcons) - horizontalPading;
export const ICON_SIZE = 30;
