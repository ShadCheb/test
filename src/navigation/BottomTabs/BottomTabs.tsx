import React from 'react';

import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';

import {TabNames} from '@constants/routeNames';
import {HeroesNavigator} from '@navigation/Heroes';
import {LeguesNavigator} from '@navigation/Legues';
import {PlayersNavigator} from '@navigation/Players';

import CustomTabBar from './CustomTabBar';

const Tab = createBottomTabNavigator();

const BottomTabs = () => {
  return (
    <Tab.Navigator
      tabBar={CustomTabBar}
      screenOptions={{
        headerShown: false,
      }}>
      <Tab.Screen name={TabNames.heroes} component={HeroesNavigator} />
      <Tab.Screen name={TabNames.players} component={PlayersNavigator} />
      <Tab.Screen name={TabNames.legues} component={LeguesNavigator} />
    </Tab.Navigator>
  );
};

export default BottomTabs;
