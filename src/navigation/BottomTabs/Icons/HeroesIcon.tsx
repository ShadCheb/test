import React from 'react';
import Svg, {Path} from 'react-native-svg';

import Theme from '@styles/theme';

import {ICON_SIZE, IconProps} from '../constants';

const Colors = Theme.Colors;


export default ({active}: IconProps) => {
  return (
    <Svg viewBox="0 0 478.53 478.53" width={ICON_SIZE} height={ICON_SIZE}>
      <Path
        d="M477.795 184.28a14.996 14.996 0 00-12.108-10.209l-147.159-21.384-65.812-133.35a15 15 0 00-26.902 0l-65.812 133.35-147.159 21.384A14.997 14.997 0 00.735 184.28a14.997 14.997 0 003.796 15.376l106.484 103.797-25.138 146.565a15.002 15.002 0 0021.765 15.813l131.623-69.199 131.623 69.199a14.988 14.988 0 006.979 1.723 15.002 15.002 0 0014.786-17.536l-25.138-146.565 106.484-103.797a14.997 14.997 0 003.796-15.376zM340.927 287.476a14.997 14.997 0 00-4.314 13.277l21.333 124.382-111.701-58.726a15.009 15.009 0 00-13.961-.001l-111.701 58.726 21.333-124.382a14.997 14.997 0 00-4.314-13.277l-90.367-88.087L172.12 181.24a15 15 0 0011.294-8.205l55.851-113.166 55.851 113.167a15 15 0 0011.294 8.205l124.884 18.148-90.367 88.087z"
        fill={active ? Colors.white : Colors.grayBlue}
        fillRule="evenodd"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </Svg>
  );
};
