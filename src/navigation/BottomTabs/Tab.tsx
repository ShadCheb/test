import React, {ReactElement, useRef} from 'react';
import {Transition, TransitioningView} from 'react-native-reanimated';

import {Item, ItemIcon, ItemTab, ItemText} from './styles';

interface IBottomTabItemProps {
  name: string;
  label: string;
  focused: boolean;
  children: ReactElement;
  onPress: () => void;
}

const Tab = ({
  name,
  label,
  focused,
  children,
  onPress,
}: IBottomTabItemProps) => {
  const ref = useRef<TransitioningView | null>(null);

  const transition = (
    <Transition.Sequence>
      <Transition.Out type="fade" durationMs={0} />
      <Transition.Change interpolation="easeInOut" durationMs={300} />
      <Transition.In type="fade" durationMs={10} />
    </Transition.Sequence>
  );

  const onPressByTab = () => {
    ref.current?.animateNextTransition();
    onPress();
  };

  return (
    <Item onPress={onPressByTab}>
      <ItemTab
        focused={focused}
        label={label}
        ref={ref}
        transition={transition}>
        <ItemIcon>{children}</ItemIcon>
        {focused && <ItemText>{name}</ItemText>}
      </ItemTab>
    </Item>
  );
};

export default Tab;
