import React from 'react';

import {BottomTabBarProps} from '@react-navigation/bottom-tabs';
import {TabNames} from 'constants/routeNames';

import {TabNamesType} from '@navigation/RootNavigator';

import {HeroesIcon, LeguesIcon, PlayersIcon} from './Icons';
import {Container} from './styles';
import TabComponent from './Tab';

type TabIconType = {active: boolean};

const tabs = {
  [TabNames.heroes]: {
    iconName: 'Герои',
    icon: ({active = false}: TabIconType) => <HeroesIcon active={active} />,
  },
  [TabNames.players]: {
    iconName: 'Игроки',
    icon: ({active = false}: TabIconType) => <PlayersIcon active={active} />,
  },
  [TabNames.legues]: {
    iconName: 'Лига',
    icon: ({active = false}: TabIconType) => <LeguesIcon active={active} />,
  },
};

const CustomTabBar = ({state, descriptors, navigation}: BottomTabBarProps) => {
  return (
    <Container edges={['bottom']}>
      {state.routes.map((route, index) => {
        const {options} = descriptors[route.key];
        const label = route.name;
        const {iconName, icon: Icon} = tabs[label as TabNamesType];

        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true,
          });

          if (!isFocused && !event.defaultPrevented) {
            // The `merge: true` option makes sure that the params inside the tab screen are preserved
            navigation.navigate({name: label, merge: true, params: undefined});
          }
        };

        return (
          <TabComponent
            key={index}
            name={iconName}
            label={label}
            focused={isFocused}
            onPress={onPress}>
            <Icon active={isFocused} />
          </TabComponent>
        );
      })}
    </Container>
  );
};

export default CustomTabBar;
