import {Transitioning} from 'react-native-reanimated';
import {SafeAreaView} from 'react-native-safe-area-context';

import styled from 'styled-components/native';

import Theme from '@styles/theme';

import {ICON_SIZE} from './constants';

type StyleItemTabType = {
  label: string;
  focused?: boolean;
};

const Colors = Theme.Colors;

export const Container = styled(SafeAreaView)`
  padding: 4px;
  flex-direction: row;
  align-items: center;
  background-color: ${Colors.blue};
`;

export const Item = styled.TouchableWithoutFeedback``;

export const ItemTab = styled(Transitioning.View)`
  flex: auto;
  padding: 6px;
  margin: 6px;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  background: ${(props: StyleItemTabType) =>
    props.focused ? Colors.grayBlue : Colors.blue};
  border-radius: 100px;
  border-width: 1px;
  border-color: ${Colors.grayBlue};
`;

export const ItemIcon = styled.View`
  width: ${ICON_SIZE}px;
  heigt: ${ICON_SIZE}px;
`;

export const ItemText = styled.Text`
  margin-left: 10px;
  color: ${Colors.white};
  font-weight: 600;
`;
