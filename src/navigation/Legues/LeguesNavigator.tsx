import React from 'react';

import {createStackNavigator} from '@react-navigation/stack';

import {RouteNames} from '@constants/routeNames';
import {Legues, Teams} from '@screens/Legues';

const Stack = createStackNavigator();

const LeguesNavigator = () => (
  <Stack.Navigator initialRouteName={RouteNames.legues}>
    <Stack.Screen name={RouteNames.legues} component={Legues} />
    <Stack.Screen name={RouteNames.teams} component={Teams} />
  </Stack.Navigator>
);

export default LeguesNavigator;
