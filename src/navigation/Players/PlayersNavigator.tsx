import React from 'react';

import {createStackNavigator} from '@react-navigation/stack';

import {RouteNames} from '@constants/routeNames';
import Players from '@screens/Players';
import Accordion from '@screens/Players/Accordion';
import SwitchButton from '@screens/Players/SwitchButton';
import SwitchButton2 from '@screens/Players/SwitchButton2';
import TestList from '@screens/Players/TestList';

const Stack = createStackNavigator();

const PlayersNavigator = () => (
  <Stack.Navigator initialRouteName={RouteNames.players}>
    <Stack.Screen name={RouteNames.players} component={Players} />
    <Stack.Screen name={RouteNames.switchButton} component={SwitchButton} />
    <Stack.Screen name={RouteNames.switchButton2} component={SwitchButton2} />
    <Stack.Screen name={RouteNames.accordion} component={Accordion} />
    <Stack.Screen name={RouteNames.testList} component={TestList} />
  </Stack.Navigator>
);

export default PlayersNavigator;
