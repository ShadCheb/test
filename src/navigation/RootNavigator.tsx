import React from 'react';

import {NavigationContainer, RouteProp} from '@react-navigation/native';
import {StackNavigationProp} from '@react-navigation/stack';

import {RouteNames, TabNames} from '@constants/routeNames';

import {BottomTabs} from './BottomTabs';

export type TabNamesType = TabNames.heroes | TabNames.legues | TabNames.players;

export type RootStackParamList = {
  [TabNames.heroes]: undefined;
  [TabNames.legues]: undefined;
  [TabNames.players]: undefined;
  [RouteNames.teams]: undefined;

  [RouteNames.switchButton]: undefined;
  [RouteNames.accordion]: undefined;
  [RouteNames.testList]: undefined;
  [RouteNames.switchButton2]: undefined;
};

export type LeguesScreenNavigationProp = StackNavigationProp<
  RootStackParamList,
  typeof TabNames.legues
>;

export type LeguesScreenRouteProp = RouteProp<
  RootStackParamList,
  typeof TabNames.legues
>;

export type TeamsScreenNavigationProp = StackNavigationProp<
  RootStackParamList,
  typeof RouteNames.teams
>;

export type TeamsScreenRouteProp = RouteProp<
  RootStackParamList,
  typeof RouteNames.teams
>;

export type PlayersScreenNavigationProp = StackNavigationProp<
  RootStackParamList,
  typeof TabNames.players
>;

export type PlayersScreenRouteProp = RouteProp<
  RootStackParamList,
  typeof TabNames.players
>;

const RootNavigator = () => (
  <NavigationContainer>
    <BottomTabs />
  </NavigationContainer>
);

export default RootNavigator;
