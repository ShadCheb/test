import React from 'react';

import {createStackNavigator} from '@react-navigation/stack';

import {RouteNames} from '@constants/routeNames';
import Heroes from '@screens/Heroes';

const Stack = createStackNavigator();

const HeroesNavigator = () => (
  <Stack.Navigator initialRouteName={RouteNames.heroes}>
    <Stack.Screen name={RouteNames.heroes} component={Heroes} />
  </Stack.Navigator>
);

export default HeroesNavigator;
