export const BrandColors = {
  white: '#ffffff',
  pink: '#dd3f99',
  red: '#ab2560',
  lightBlue: '#e5e8fe',
  grayBlue: '#8299b3',
  blue: '#35547b',
  black: '#1c1c1c',
};
