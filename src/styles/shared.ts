import {Platform} from 'react-native';

import {css} from 'styled-components/native';

export default {
  shadowItem:
    Platform.OS === 'ios'
      ? css`
        shadow-color: #1c1c1c;
        shadow-offset: {width: 0, height: 2};
        shadow-opacity: 0.15;
        shadow-radius: 3.84;
      `
      : css`
          elevation: 5;
          shadow-color: #1c1c1c;
        `,
};
