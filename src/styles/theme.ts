import {BrandColors} from './colors';
import {fontScale} from './mixins';

// TODO: Добавить FontWeight
export default {
  Colors: BrandColors,
  FontSizes: {
    size_28: fontScale(28),
    size_22: fontScale(22),
    size_20: fontScale(20),
    size_18: fontScale(18),
    size_17: fontScale(17),
    size_16: fontScale(16),
    size_14: fontScale(14),
    size_13: fontScale(13),
    size_12: fontScale(12),
    size_11: fontScale(11),
    size_10: fontScale(10),
    size_9: fontScale(9),
    size_8: fontScale(8),
  },
  LineHeights: {
    height_34: fontScale(34),
    height_32: fontScale(32),
    height_24: fontScale(24),
    height_22: fontScale(22),
    height_20: fontScale(20),
    height_18: fontScale(18),
    height_17: fontScale(17),
    height_16: fontScale(16),
    height_14: fontScale(14),
    height_12: fontScale(12),
    height_11: fontScale(11),
    height_10: fontScale(10),
  },
  LetterSpacing: 0.36,
  BorderRadius: 12,
};
