import {Dimensions, PixelRatio} from 'react-native';

const {width, height} = Dimensions.get('window');

// SCALING
const guidelineBaseHeight = 568;
const guidelineBaseWidth = 320;

const verticalRatio = height / guidelineBaseHeight;
const horizontalRatio = width / guidelineBaseWidth;

export const verticalScale = (size: number): number =>
  Math.round(PixelRatio.roundToNearestPixel(verticalRatio * size));

export const horizontalScale = (size: number): number =>
  Math.round(PixelRatio.roundToNearestPixel(horizontalRatio * size));

export const moderateScale = (size: number, factor = 0.5): number =>
  Math.round(size + (horizontalScale(size) - size) * factor);

export const fontScale = (size: number): number =>
  moderateScale(size) * PixelRatio.getFontScale();
