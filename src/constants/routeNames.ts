export enum RouteNames {
  heroes = 'Heroes',
  players = 'Players',
  legues = 'Legues',
  teams = 'Teams',

  // players
  switchButton = 'SwitchButton',
  accordion = 'Accordion',
  testList = 'TestList',
  switchButton2 = 'SwitchButton2',
}

export enum TabNames {
  heroes = 'HeroesTab',
  players = 'PlayersTab',
  legues = 'LeguesTab',
}
