export class TextHelper {

  static getFirstLetterUppercase = (text: string): string => {
    return text[0].toUpperCase() + text.slice(1);
  }

}