import React, {FC} from 'react';
import {LogBox} from 'react-native';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {Provider} from 'react-redux';

import {applyMiddleware, createStore} from 'redux';
import thunk from 'redux-thunk';

// import {ThemeProvider} from 'styled-components/native';
import {RootNavigator} from './navigation';
import {rootReducer} from './store';

LogBox.ignoreLogs(['Warning: ...']); // Игнорировать уведомление журнала по сообщению
// LogBox.ignoreAllLogs(); // Игнорировать все уведомления журнала

const store = createStore(rootReducer, applyMiddleware(thunk));

export const App: FC = () => (
  <Provider store={store}>
    <SafeAreaProvider>
      <RootNavigator />
    </SafeAreaProvider>
  </Provider>
);
