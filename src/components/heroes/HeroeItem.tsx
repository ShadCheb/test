import React from 'react';

import styled from 'styled-components/native';

import {Text, TextEnum} from '@components/ui/Text';
import {API_ENDPOINT} from '@constants/endpoints';
import {IHero} from '@store/heroes/types';
import SharedStyle from '@styles/shared';
import Theme from '@styles/theme';

type PropsType = {
  item: IHero;
};

const AVATAR_HEIGHT = 140;
const ITEM_HEIGHT = AVATAR_HEIGHT + 40;
const BORDER_RADIUS = 12;
const Colors = Theme.Colors;

const HeroeItem = ({item}: PropsType) => {
  return (
    <Item>
      <Avatar>
        <AvatarImage
          source={{uri: `${API_ENDPOINT}${item.img}`}}
          resizeMode="cover"
        />
      </Avatar>
      <Description>
        <Text Ag={TextEnum.SUBTITLE} numberOfLines={1}>
          {item.localized_name}
        </Text>
      </Description>
    </Item>
  );
};

const Avatar = styled.View`
  height: ${AVATAR_HEIGHT}px;
  border-radius: ${BORDER_RADIUS}px;
  background-color: ${Colors.white};
  overflow: hidden;
`;

const AvatarImage = styled.Image`
  height: ${ITEM_HEIGHT}px;
`;

const Description = styled.View`
  paddin-vertical: 4px;
  padding-horizontal: 6px;
  margin-top: 5px;
  align-items: center;
`;

const Item = styled.View`
  margin-vertical: 15px;
  margin-horizontal: 15px;
  border-radius: ${BORDER_RADIUS}px;
  height: ${ITEM_HEIGHT}px;
  background-color: ${Colors.lightBlue};
  ${SharedStyle.shadowItem};
`;

export default HeroeItem;
