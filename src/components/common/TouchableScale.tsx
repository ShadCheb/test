import React, {ReactElement} from 'react';
import Animated, {
  Extrapolate,
  interpolate,
  useAnimatedStyle,
  useDerivedValue,
  useSharedValue,
  withTiming,
} from 'react-native-reanimated';

import styled from 'styled-components/native';

type PropsType = {
  children: ReactElement;
  onPress: () => void;
  scaleTo?: number;
  disabled?: boolean;
};

const TimingConfig = {duration: 50};

export const TouchableScale = ({
  children,
  onPress,
  scaleTo = 0.97,
  disabled = false,
}: PropsType) => {
  const pressed = useSharedValue(false);
  const progress = useDerivedValue(() => {
    return pressed.value
      ? withTiming(1, TimingConfig)
      : withTiming(0, TimingConfig);
  });
  const animatedStyle = useAnimatedStyle(() => {
    const scale = interpolate(
      progress.value,
      [0, 1],
      [1, scaleTo],
      Extrapolate.CLAMP,
    );

    return {
      transform: [{scale}],
    };
  });

  const onPressIn = () => {
    pressed.value = true;
  };

  const onPressOut = () => {
    pressed.value = false;
  };

  return (
    <Block
      onPressIn={onPressIn}
      onPressOut={onPressOut}
      onPress={onPress}
      disabled={disabled}>
      <Animated.View style={animatedStyle}>{children}</Animated.View>
    </Block>
  );
};

const Block = styled.TouchableWithoutFeedback``;
