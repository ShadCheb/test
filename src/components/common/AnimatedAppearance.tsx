import React, {ReactElement, useEffect} from 'react';
import Animated, {
  interpolate,
  useAnimatedStyle,
  useDerivedValue,
  useSharedValue,
  withDelay,
  withTiming,
} from 'react-native-reanimated';

type PropsType = {
  children: ReactElement;
  index?: number;
};

export const AnimatedAppearance = ({children, index}: PropsType) => {
  const play = useSharedValue(false);
  const progress = useDerivedValue(() => {
    return play.value
      ? withDelay(50 * (index ?? 0), withTiming(1, {duration: 350}))
      : 0;
  });

  useEffect(() => {
    play.value = true;
  }, []); // eslint-disable-line

  const animatedStyle = useAnimatedStyle(() => {
    const opacity = interpolate(progress.value, [0, 1], [0, 1]);

    const translateY = interpolate(progress.value, [0, 1], [100, 0]);

    return {
      opacity,
      transform: [{translateY}],
    };
  });

  return <Animated.View style={animatedStyle}>{children}</Animated.View>;
};
