import React from 'react';

import styled from 'styled-components/native';

import {AnimatedAppearance} from '@components/common/AnimatedAppearance';
import {Text, TextEnum} from '@components/ui/Text';
import type {ITeamByLegue} from '@store/legues/types';
import SharedStyle from '@styles/shared';
import Theme from '@styles/theme';

type PropsType = {
  item: ITeamByLegue;
  index: number;
};

const ITEM_HEIGHT = 120;
const BORDER_RADIUS = 12;
const Colors = Theme.Colors;

const LegueItem = ({item, index}: PropsType) => {
  return (
    <AnimatedAppearance index={index}>
      <Item>
        <Description>
          <DescriptionHeader>
            <Text Ag={TextEnum.P} numberOfLines={1}>
              #{item.team_id}
            </Text>
            <Text Ag={TextEnum.P}>{item.rating}</Text>
          </DescriptionHeader>
          <DescriptionBody>
            <Text Ag={TextEnum.SUBTITLE} numberOfLines={1}>
              {item.name}
            </Text>
          </DescriptionBody>
          <DescriptionFooter>
            <Text Ag={TextEnum.DESCRIPTION}>
              {item.losses}/{item.wins}
            </Text>
          </DescriptionFooter>
        </Description>
        <Avatar>
          <AvatarImage source={{uri: item.logo_url}} resizeMode="center" />
        </Avatar>
      </Item>
    </AnimatedAppearance>
  );
};

const Item = styled.View`
  margin-vertical: 15px;
  margin-horizontal: 15px;
  flex-direction: row;
  border-radius: ${BORDER_RADIUS}px;
  height: ${ITEM_HEIGHT}px;
  background-color: ${Colors.lightBlue};
  ${SharedStyle.shadowItem};
`;

const Avatar = styled.View`
  border-radius: ${BORDER_RADIUS}px;
  background-color: ${Colors.white};
  overflow: hidden;
`;

const AvatarImage = styled.Image`
  height: ${ITEM_HEIGHT}px;
  width: ${ITEM_HEIGHT}px;
`;

const Description = styled.View`
  flex: 1;
  padding-vertical: 10px;
  padding-left: 20px;
  padding-right: 10px;
  back
`;

const DescriptionHeader = styled.View`
  flex-direction: row;
  justify-content: space-between;
`;

const DescriptionBody = styled.View`
  flex: 1;
  align-items: center;
  justify-content: center;
`;

const DescriptionFooter = styled.View`
  align-items: flex-end;
`;

export default LegueItem;
