import React from 'react';

import styled from 'styled-components/native';

import {AnimatedAppearance} from '@components/common/AnimatedAppearance';
import {TouchableScale} from '@components/common/TouchableScale';
import {Text, TextEnum} from '@components/ui/Text';
import type {ILegue} from '@store/legues/types';
import SharedStyle from '@styles/shared';
import Theme from '@styles/theme';

type PropsType = {
  item: ILegue;
  onPress(item: ILegue): void;
  index: number;
};

const ITEM_HEIGHT = 80;
const BORDER_RADIUS = 12;
const Colors = Theme.Colors;

const LegueItem = ({item, onPress, index}: PropsType) => {
  return (
    <AnimatedAppearance index={index}>
      <TouchableScale onPress={() => onPress(item)} scaleTo={0.97}>
        <Item>
          <Description>
            <Text Ag={TextEnum.P} numberOfLines={1}>
              {item.name}
            </Text>
          </Description>
        </Item>
      </TouchableScale>
    </AnimatedAppearance>
  );
};

const Item = styled.View`
  margin-vertical: 15px;
  margin-horizontal: 15px;
  border-radius: ${BORDER_RADIUS}px;
  height: ${ITEM_HEIGHT}px;
  background-color: ${Colors.lightBlue};
  ${SharedStyle.shadowItem};
`;

const Description = styled.View`
  padding-vertical: 10px;
  padding-left: 20px;
  padding-right: 10px;
  justify-content: center;
`;

export default LegueItem;
