// TODO: Доделать тест для react native reanimated
import React from 'react';
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
// import {withReanimatedTimer} from 'react-native-reanimated/lib/reanimated2/jestUtils';
import renderer from 'react-test-renderer';

import LegueItem from '../LegueItem';

const legue = {
  banner: null,
  leagueid: 11894,
  name: 'adsf',
  ticket: null,
  tier: 'premium',
};

jest.useFakeTimers();
jest.runAllTimers();

test(`renders LegueItem`, () => {
  const onSelectLegue = jest.fn();
  const tree = renderer.create(
    <LegueItem item={legue} onPress={onSelectLegue} index={0} />,
  );
  expect(tree).toMatchSnapshot();

  // withReanimatedTimer(() => {
  //   const onPress = jest.fn();

  //   const tree = renderer.create(
  //     <LegueItem item={legue} index={0} onPress={onPress} />,
  //   );
  //   expect(tree).toMatchSnapshot();
  // });
});
