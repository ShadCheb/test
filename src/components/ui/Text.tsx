import React from 'react';
import { StyleSheet, Text as RNText, TextProps } from 'react-native';

import Theme from 'styles/theme';

const { Colors, FontSizes, LineHeights, LetterSpacing } = Theme;

export enum TextEnum {
  H1 = 'h1',
  SUBTITLE = 'subtitle',
  P = 'paragraph',
  DESCRIPTION = 'description',
}

interface IText extends TextProps {
  Ag: TextEnum;
  children?: string | number | React.ReactNode[];
  align?: 'auto' | 'left' | 'center' | 'justify';
  color?: string;
  numberOfLines?: number;
}

export const Text = (props: IText) => {
  const {
    Ag,
    color = Colors.black,
    align = 'auto',
    children,
    numberOfLines = 0,
  } = props;

  return (
    <RNText
      style={[
        styles[Ag],
        {
          color: color,
          textAlign: align,
        },
      ]}
      numberOfLines={numberOfLines}>
      {children}
    </RNText>
  );
};

const styles = StyleSheet.create({
  [TextEnum.H1]: {
    fontSize: FontSizes.size_20,
    lineHeight: LineHeights.height_34,
    letterSpacing: LetterSpacing,
    fontWeight: '700',
  },
  [TextEnum.SUBTITLE]: {
    fontSize: FontSizes.size_17,
    lineHeight: LineHeights.height_22,
    letterSpacing: LetterSpacing,
    fontWeight: '700',
  },
  [TextEnum.P]: {
    fontSize: FontSizes.size_14,
    lineHeight: LineHeights.height_18,
    fontWeight: '400',
  },
  [TextEnum.DESCRIPTION]: {
    fontSize: FontSizes.size_12,
    lineHeight: LineHeights.height_16,
    fontWeight: '400',
    opacity: 0.7,
  },
});
