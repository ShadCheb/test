import React, {useEffect, useState} from 'react';
import {StyleSheet} from 'react-native';
import Animated, {
  cancelAnimation,
  Easing,
  interpolate,
  useAnimatedStyle,
  useSharedValue,
  withRepeat,
  withTiming,
} from 'react-native-reanimated';

import styled from 'styled-components/native';

interface CircleProps {
  borderColor: string;
  additionalRadius?: number;
  rotationOffset?: number;
  rotation?: Animated.SharedValue<number>;
}

const CIRCLE_RADIUS = 100;
const CIRCLE_BORDER_WITH = 2;
const ADDITIONAL_RADIUS = 6;

const LoadingText = () => {
  const [counter, setCounter] = useState(1);
  const MAX_DOTS = 3;

  useEffect(() => {
    const intervalID = setInterval(() => {
      setCounter((counter % MAX_DOTS) + 1);
    }, 750);
    return () => clearInterval(intervalID);
  }, [counter]);

  const dots = '.'.repeat(counter);
  const spaces = ' '.repeat(MAX_DOTS - counter);

  return (
    <LoadingBlock>
      <LoadingBlockText>Загрузка</LoadingBlockText>
      <LoadingBlockText>{dots + spaces}</LoadingBlockText>
    </LoadingBlock>
  );
};

const Circle: React.FC<CircleProps> = props => {
  const additionalRadius = props.additionalRadius ?? 0;
  const radius = CIRCLE_RADIUS + additionalRadius;
  const additionalOffset = props.additionalRadius ? CIRCLE_BORDER_WITH : 0;
  const degreeOffset = props.rotationOffset ? props.rotationOffset : 0;

  const animatedStyle = useAnimatedStyle(() => {
    const rotationDegree = interpolate(
      props.rotation ? props.rotation.value : 0,
      [0, 1],
      [0 + degreeOffset, 360 + degreeOffset],
    );

    return {
      transform: [
        {rotate: `${rotationDegree}deg`},
        {translateX: -additionalRadius + additionalOffset},
      ],
    };
  });

  return (
    <Animated.View
      style={[
        styles.circle,
        {
          height: radius * 2,
          width: radius * 2,
          borderRadius: radius,
          borderColor: props.borderColor,
        },
        animatedStyle,
      ]}
    />
  );
};

const Loading = () => {
  const rotation = useSharedValue(0);
  const startAnimation = () => {
    rotation.value = 0;
    rotation.value = withRepeat(
      withTiming(1, {duration: 1500, easing: Easing.linear}),
      -1, // inifinite
      false, // do not reverse
    );
  };

  useEffect(() => {
    startAnimation();
    return () => {
      cancelAnimation(rotation);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Container>
      <ContainerCircle>
        <Circle borderColor="rgb(50, 150, 110)" />
        <Circle
          borderColor="rgb(50, 150, 200)"
          additionalRadius={ADDITIONAL_RADIUS}
          rotation={rotation}
        />
        <Circle
          borderColor="rgb(220, 0, 90)"
          additionalRadius={ADDITIONAL_RADIUS}
          rotation={rotation}
          rotationOffset={60}
        />
      </ContainerCircle>
      <LoadingText />
    </Container>
  );
};

const Container = styled.View`
  position: absolute;
  top: 0;
  let: 0;
  justify-content: center;
  align-items: center;
  background-color: white;
  width: 100%;
  height: 100%;
  z-index: 100;
`;

const ContainerCircle = styled.View`
  justify-content: center;
  align-items: center;
  height: ${(CIRCLE_RADIUS + ADDITIONAL_RADIUS) * 2 +
  ADDITIONAL_RADIUS * 2 -
  CIRCLE_BORDER_WITH * 2}px;
  width: 100%;
`;

const LoadingBlock = styled.View`
  flex-direction: row;
  margin-top: 12;
`;

const LoadingBlockText = styled.Text``;

const styles = StyleSheet.create({
  circle: {
    position: 'absolute',
    width: CIRCLE_RADIUS * 2,
    height: CIRCLE_RADIUS * 2,
    borderRadius: CIRCLE_RADIUS,
    backgroundColor: 'rgba(0, 0, 0, 0)',
    borderWidth: CIRCLE_BORDER_WITH,
    borderColor: '#F00',
  },
});

export default Loading;
