import React from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {
  Transition,
  Transitioning,
  TransitioningView,
} from 'react-native-reanimated';

const data = [
  {
    bg: '#A8DDE9',
    color: '#3F5B98',
    category: 'Healthcare',
    subCategories: ['Skincare', 'Personal care', 'Health', 'Eye care'],
  },
  {
    bg: '#086E4B',
    color: '#FCBE4A',
    category: 'Food & Drink',
    subCategories: [
      'Fruits & Vegetables',
      'Frozen Food',
      'Bakery',
      'Snacks & Desserts',
      'Beverages',
      'Alcoholic beverages',
      'Noodles & Pasta',
      'Rice & Cooking oil',
    ],
  },
  {
    bg: '#FECBCA',
    color: '#FD5963',
    category: 'Beauty',
    subCategories: ['Skincare', 'Makeup', 'Nail care', 'Perfume'],
  },
  {
    bg: '#193B8C',
    color: '#FECBCD',
    category: 'Baby & Kids',
    subCategories: [
      'Toys',
      'Trolleys',
      'LEGO®',
      'Electronics',
      'Puzzles',
      'Costumes',
      'Food',
      'Hygiene & Care',
      "Child's room",
      'Feeding accessories',
    ],
  },
  {
    bg: '#FDBD50',
    color: '#F5F5EB',
    category: 'Homeware',
    subCategories: [
      'Air purifiers',
      'Stoves, hoods & ovens',
      'Refrigerators',
      'Coffee & Tea',
      'Air conditioning',
      'Grilling',
      'Vacuum cleaners',
    ],
  },
];

const transition = (
  <Transition.Together>
    <Transition.In type="fade" durationMs={200} />
    <Transition.Change />
    <Transition.Out type="fade" durationMs={200} />
  </Transition.Together>
);

export default function App() {
  const [currentIndex, setCurrentIndex] = React.useState<number | null>(null);
  const ref = React.useRef<TransitioningView | null>(null);

  return (
    <Transitioning.View
      ref={ref}
      transition={transition}
      style={styles.container}>
      {data.map(({bg, color, category, subCategories}, index) => {
        return (
          <TouchableOpacity
            key={category}
            onPress={() => {
              ref.current?.animateNextTransition();
              setCurrentIndex(index === currentIndex ? null : index);
            }}
            style={styles.cardContainer}
            activeOpacity={0.9}>
            <View style={[styles.card, {backgroundColor: bg}]}>
              <Text style={[styles.heading, {color}]}>{category}</Text>
              {index === currentIndex && (
                <View style={styles.subCategoriesList}>
                  {subCategories.map(subCategory => (
                    <Text key={subCategory} style={[styles.body, {color}]}>
                      {subCategory}
                    </Text>
                  ))}
                </View>
              )}
            </View>
          </TouchableOpacity>
        );
      })}
    </Transitioning.View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
  },
  cardContainer: {
    flexGrow: 1,
  },
  card: {
    flexGrow: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  heading: {
    fontSize: 38,
    fontWeight: '900',
    textTransform: 'uppercase',
    letterSpacing: -2,
  },
  body: {
    fontSize: 20,
    lineHeight: 20 * 1.5,
    textAlign: 'center',
  },
  subCategoriesList: {
    marginTop: 20,
  },
});
