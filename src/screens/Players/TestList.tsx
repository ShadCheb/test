import React, {useEffect, useState} from 'react';

// import {Button} from 'react-native';
import styled from 'styled-components/native';

import {Text, TextEnum} from '@components/ui/Text';
import Theme from '@styles/theme';

import data from './testData';

const Colors = Theme.Colors;

const ClockBlock = ({count, onAddCount, onSubCount, onSortByAge}: any) => (
  <Count>
    <CountBtns>
      <Button onPress={onAddCount}>
        <Text Ag={TextEnum.P}>+1</Text>
      </Button>
      <Button onPress={onSubCount}>
        <Text Ag={TextEnum.P}>-1</Text>
      </Button>
      <Button onPress={onSortByAge}>
        <Text Ag={TextEnum.P}>Age</Text>
      </Button>
    </CountBtns>
    <Text Ag={TextEnum.P}>{count}</Text>
  </Count>
);

const ItemBlock = ({items}: any) => {
  // useEffect(() => {
  //   console.log('===========ITEMS', items.map(item => item.age));
  // }, [items]);

  // key={item._id}
  return (
    <>
      {items.map((item: any, idx: number) => (
        <Item>
          <ItemBody>
            <Text Ag={TextEnum.SUBTITLE}>
              {item.name}, {item.age}
            </Text>
            <Text Ag={TextEnum.P}>{item.phone}</Text>
            <Text Ag={TextEnum.DESCRIPTION}>{item.email}</Text>
          </ItemBody>
          <ItemStatus isActive={item.isActive} />
        </Item>
      ))}
    </>
  );
};

export default function TestList() {
  const [count, setCount] = useState(0);
  const [items, setItems] = useState([...data]);

  const onAddCount = () => {
    console.log('==============ADD');

    console.log('===========ITEMS', data.map(item => item.age));

    setItems([...data]);
    setCount(count + 1);
  };

  const onSubCount = () => {
    console.log('==============SUB');
    setCount(count - 1);
  };

  const onSortByAge = () => {
    const sortItems = items.sort((itemA, itemB) => {
      if (itemA.age > itemB.age) return -1;
      if (itemA.age < itemB.age) return 1;
      return 0;
    });

    console.log('SORT', items.map(item => item.age));

    setItems([...sortItems]);
    setCount(0);
  };

  return (
    <>
      <ClockBlock
        count={count}
        onAddCount={onAddCount}
        onSubCount={onSubCount}
        onSortByAge={onSortByAge}
      />
      <Container>
        <ItemBlock items={items} />
        <ShiftBlock />
      </Container>
    </>
  );
}

const Count = styled.View`
  padding: 10px;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

const Button = styled.TouchableOpacity`
  padding-vertical: 5px;
  padding-horizontal: 10px;
  margin-left: 2px;
  margin-right: 2px;
  background-color: ${Colors.lightBlue};
  color: ${Colors.grayBlue};
`;

const CountBtns = styled.View`
  flex: 1;
  flex-direction: row;
  margin-left: -2px;
  margin-right: -2px;
`;

const Container = styled.ScrollView`
  flex: 1;
  padding-horizontal: 10px;
  padding-top: 20px;
  padding-bottom: 50px;
`;

const Item = styled.View`
  flex-direction: row;
  justify-content: space-between;
  margin-bottom: 10px;
  border-width: 1px;
  border-color: ${Colors.blue};
`;

const ItemBody = styled.View`
  padding: 10px;
`;

const ItemStatus = styled.View`
  width: 10px;
  height: 100%;
  background-color: ${({isActive}: {isActive: boolean}) =>
    isActive ? 'green' : 'grey'};
`;

const ShiftBlock = styled.View`
  width: 100%;
  height: 50px;
`;
