import React from 'react';

import {Text, TextEnum} from 'components/ui/Text';
import styled from 'styled-components/native';

import {RouteNames} from '@constants/routeNames';
import {
  PlayersScreenNavigationProp,
  PlayersScreenRouteProp,
} from '@navigation/RootNavigator';
import Theme from '@styles/theme';

type PlayersPropsType = {
  route: PlayersScreenRouteProp;
  navigation: PlayersScreenNavigationProp;
};

const Colors = Theme.Colors;

const PlayersScreen = ({route, navigation}: PlayersPropsType) => {
  const onNavigateSwitchButton = () => {
    navigation.navigate(RouteNames.switchButton);
  };

  const onNavigateSwitchButton2 = () => {
    navigation.navigate(RouteNames.switchButton2);
  };

  const onNavigateAccordion = () => {
    navigation.navigate(RouteNames.accordion);
  };

  const onNavigateTestList = () => {
    navigation.navigate(RouteNames.testList);
  };

  return (
    <Container>
      <Link onPress={onNavigateSwitchButton}>
        <Text Ag={TextEnum.SUBTITLE}>Switch Button</Text>
      </Link>
      <Link onPress={onNavigateSwitchButton2}>
        <Text Ag={TextEnum.SUBTITLE}>Switch Button 2</Text>
      </Link>
      <Link onPress={onNavigateAccordion}>
        <Text Ag={TextEnum.SUBTITLE}>Accordion</Text>
      </Link>
      <Link onPress={onNavigateTestList}>
        <Text Ag={TextEnum.SUBTITLE}>TestList</Text>
      </Link>
    </Container>
  );
};

const Container = styled.View`
  flex: 1;
  padding: 15px;
`;

const Link = styled.TouchableOpacity`
  padding-vertical: 10px;
  padding-horizontal: 20px;
  margin-bottom: 20px;
  width: 100%;
  border-bottom-width: 1px;
  border-color: ${Colors.grayBlue};
`;

export default PlayersScreen;
