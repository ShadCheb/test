import React, {useMemo, useState} from 'react';
import {Easing} from 'react-native-reanimated';

import {AnimatePresence, MotiTransitionProp, MotiView} from 'moti';
import styled from 'styled-components/native';

import Theme from '@styles/theme';

const Colors = Theme.Colors;
const SIZE = 60;

const transitionSwitchBase: MotiTransitionProp = {
  type: 'timing',
  duration: 300,
  easing: Easing.inOut(Easing.ease),
};
const transitionSwitchBtn: MotiTransitionProp = {
  type: 'timing',
  duration: 300,
  easing: Easing.inOut(Easing.ease),
  translateX: {
    type: 'spring',
    delay: 100,
  },
};

type SwitchType = {
  size: number;
  isActive: boolean;
  onPress: () => void;
};

const Switch = (props: SwitchType) => {
  const {size, isActive, onPress} = props;
  const {trackWidth, trackHeight, knobSize} = useMemo(() => {
    return {
      trackWidth: size * 1.5,
      trackHeight: size * 0.5,
      knobSize: size * 0.6,
    };
  }, [size]);

  return (
    <SwitchBlock onPress={onPress}>
      <MotiBase
        transition={transitionSwitchBase}
        from={{
          backgroundColor: isActive ? Colors.red : Colors.pink,
        }}
        animate={{
          backgroundColor: isActive ? Colors.pink : Colors.red,
        }}
        width={trackWidth}
        height={trackHeight}
      />
      <MotiBtn
        transition={transitionSwitchBtn}
        animate={{
          translateX: isActive ? trackWidth / 4 : -trackWidth / 4,
        }}
        size={trackWidth * 0.7}>
        <MotiSign
          transition={transitionSwitchBase}
          animate={{
            width: isActive ? 0 : knobSize,
          }}
          size={knobSize}
        />
      </MotiBtn>
    </SwitchBlock>
  );
};

const SwitchButton2 = () => {
  const [isVisibleSwitch, onVisibleSwitch] = useState(true);
  const [isActive, onActive] = useState(false);

  const changeActive = () => {
    onActive(!isActive);
  };

  return (
    <Container>
      <AnimatePresence>
        {isVisibleSwitch && (
          <Switch isActive={isActive} size={SIZE} onPress={changeActive} />
        )}
      </AnimatePresence>
      <Settings>
        <Button
          title="Show switch"
          onPress={() => onVisibleSwitch(!isVisibleSwitch)}
        />
      </Settings>
    </Container>
  );
};

export default SwitchButton2;

type styleMotiBasetype = {
  width: number;
  height: number;
};

type StyleMotiBtnType = {
  size: number;
};

const Container = styled.View`
  flex: 1;
`;

const SwitchBlock = styled.Pressable`
  align-items: center;
  justify-content: center;
`;

const MotiBase = styled(MotiView)`
  position: absolute;
  width: ${({width}: styleMotiBasetype) => width}px;
  height: ${({height}) => height}px;
  border-radius: ${({height}) => height / 2}px;
`;

const MotiBtn = styled(MotiView)`
  width: ${({size}: StyleMotiBtnType) => size}px;
  height: ${({size}) => size}px;
  border-radius: ${({size}) => size / 2}px;
  background-color: ${Colors.lightBlue};
  align-items: center;
  justify-content: center;
`;

const MotiSign = styled(MotiView)`
  width: ${({size}: StyleMotiBtnType) => size}px;
  height: ${({size}) => size}px;
  border-radius: ${({size}) => size / 2}px;
  border-width: ${({size}) => size * 0.1}px;
  border-color: ${Colors.black};
`;

const Settings = styled.View`
  margin-top: 20px;
`;

const Button = styled.Button``;
