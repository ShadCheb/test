import React from 'react';
import {FlatList, ListRenderItemInfo} from 'react-native';
import {connect} from 'react-redux';

import styled from 'styled-components/native';

import HeroeItem from '@components/heroes/HeroeItem';
import {getHeroes} from '@store/heroes/actions';
import {IHero} from '@store/heroes/types';
import {AppStateType} from '@store/index';

interface PropsFromState {
  loading: boolean;
  data: IHero[];
  errors?: string;
}

interface PropsFromDispatch {
  getHeroes: typeof getHeroes;
}

type AllProps = PropsFromState & PropsFromDispatch;

class Heroes extends React.Component<AllProps> {
  public componentDidMount() {
    const {getHeroes} = this.props;

    getHeroes();
  }

  renderItem = ({item}: ListRenderItemInfo<IHero>) => {
    return <HeroeItem item={item} />;
  };

  public render() {
    const {data} = this.props;

    return (
      <Container>
        <FlatList
          contentContainerStyle={{paddingTop: 15}}
          data={data}
          keyExtractor={item => `legue_${item.id}`}
          renderItem={this.renderItem}
        />
      </Container>
    );
  }
}

const mapStateToProps = ({heroes}: AppStateType) => ({
  loading: heroes.loading,
  errors: heroes.errors,
  data: heroes.data,
});

const mapDispatchToProps = {
  getHeroes,
};

const Container = styled.View`
  flex: 1;
`;

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
export default connect(mapStateToProps, mapDispatchToProps)(Heroes);
