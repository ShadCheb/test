import React from 'react';
import {SafeAreaProvider} from 'react-native-safe-area-context';
import {Provider} from 'react-redux';
import renderer from 'react-test-renderer';

import {fireEvent} from '@testing-library/react-native';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';

import Heroes from '../Heroes';

const heroes = [
  {
    '1_pick': 26439,
    '1_win': 13313,
    '2_pick': 45449,
    '2_win': 22982,
    '3_pick': 52960,
    '3_win': 27190,
    '4_pick': 43246,
    '4_win': 22235,
    '5_pick': 24501,
    '5_win': 12390,
    '6_pick': 10481,
    '6_win': 5353,
    '7_pick': 4322,
    '7_win': 2166,
    '8_pick': 1246,
    '8_win': 660,
    agi_gain: 2.8,
    attack_range: 150,
    attack_rate: 1.4,
    attack_type: 'Melee',
    base_agi: 24,
    base_armor: 0,
    base_attack_max: 33,
    base_attack_min: 29,
    base_health: 200,
    base_health_regen: 0.25,
    base_int: 12,
    base_mana: 75,
    base_mana_regen: 0,
    base_mr: 25,
    base_str: 23,
    cm_enabled: true,
    hero_id: 1,
    icon: '/apps/dota2/images/dota_react/heroes/icons/antimage.png?',
    id: 1,
    img: '/apps/dota2/images/dota_react/heroes/antimage.png?',
    int_gain: 1.8,
    legs: 2,
    localized_name: 'Anti-Mage',
    move_speed: 310,
    name: 'npc_dota_hero_antimage',
    null_pick: 1919963,
    null_win: 0,
    primary_attr: 'agi',
    pro_ban: 133,
    pro_pick: 46,
    pro_win: 22,
    projectile_speed: 0,
    roles: ['Carry', 'Escape', 'Nuker'],
    str_gain: 1.6,
    turbo_picks: 135878,
    turbo_wins: 70632,
    turn_rate: null,
  },
  {
    '1_pick': 25282,
    '1_win': 13254,
    '2_pick': 42667,
    '2_win': 21778,
    '3_pick': 46912,
    '3_win': 23842,
    '4_pick': 36070,
    '4_win': 17979,
    '5_pick': 18915,
    '5_win': 9321,
    '6_pick': 7358,
    '6_win': 3577,
    '7_pick': 2739,
    '7_win': 1307,
    '8_pick': 564,
    '8_win': 258,
    agi_gain: 2.2,
    attack_range: 150,
    attack_rate: 1.7,
    attack_type: 'Melee',
    base_agi: 20,
    base_armor: -1,
    base_attack_max: 31,
    base_attack_min: 27,
    base_health: 200,
    base_health_regen: 2.75,
    base_int: 18,
    base_mana: 75,
    base_mana_regen: 0,
    base_mr: 25,
    base_str: 25,
    cm_enabled: true,
    hero_id: 2,
    icon: '/apps/dota2/images/dota_react/heroes/icons/axe.png?',
    id: 2,
    img: '/apps/dota2/images/dota_react/heroes/axe.png?',
    int_gain: 1.6,
    legs: 2,
    localized_name: 'Axe',
    move_speed: 310,
    name: 'npc_dota_hero_axe',
    null_pick: 2271872,
    null_win: 0,
    primary_attr: 'str',
    pro_ban: 17,
    pro_pick: 32,
    pro_win: 11,
    projectile_speed: 900,
    roles: ['Initiator', 'Durable', 'Disabler', 'Jungler', 'Carry'],
    str_gain: 3.4,
    turbo_picks: 106029,
    turbo_wins: 53865,
    turn_rate: null,
  },
];

const initialState = {
  heroes: {
    loading: false,
    errors: undefined,
    data: heroes,
  },
};
const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);
const store = mockStore(initialState);

test('renders auth', () => {
  // начальные props компонента
  const mockGetHeroes = jest.fn();
  const props = {
    getHeroes: mockGetHeroes,
  };

  const auth = renderer
    .create(
      <Provider store={store}>
        <SafeAreaProvider
          initialSafeAreaInsets={{top: 1, left: 2, right: 3, bottom: 4}}>
          <Heroes {...props} />
        </SafeAreaProvider>
      </Provider>,
    )
    .toJSON();

  expect(auth).toMatchSnapshot();
});

// test('auth', async () => {
//   const mockEmailChanged = jest.fn();
//   const mockPasswordChanged = jest.fn();
//   const mockLoginUser = jest.fn();
//   const mockGetUserData = jest.fn();

//   // начальные props компонента
//   const props = {
//     emailChanged: mockEmailChanged,
//     passwordChanged: mockPasswordChanged,
//     loginUser: mockLoginUser,
//     getUserData: mockGetUserData,
//   }

//   const {getByTestId, getByText, queryByTestId, toJSON} = render(
//     <Container
//       store={store}
//     >
//       <Auth { ...props } />
//     </Container>
//   );

//   const errorEmail = getByTestId('errorEmail');
//   const errorPassword = getByTestId('errorPassword');
//   const buttonComeIn = getByTestId('buttonComeIn');

//   fireEvent.press(buttonComeIn);
//   expect(errorEmail).toBeTruthy();
//   expect(errorPassword).toBeTruthy();
//   expect(errorEmail.props.children).toBe(null);
//   expect(errorPassword.props.children).toBe(null)
// })
