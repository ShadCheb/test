import React, {useEffect, useRef} from 'react';
import {FlatList, ListRenderItemInfo} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';

import styled from 'styled-components/native';

import LegueItem from '@components/legues/LegueItem';
import Loading from '@components/ui/Loading';
import {RouteNames} from '@constants/routeNames';
import {
  LeguesScreenNavigationProp,
  LeguesScreenRouteProp,
} from '@navigation/RootNavigator';
import {getLegues, selectLegue} from '@store/legues/actions';
import {legues, leguesInfo} from '@store/legues/selectors';
import {ILegue} from '@store/legues/types';
import Theme from '@styles/theme';

type LeguesPropsType = {
  route: LeguesScreenRouteProp;
  navigation: LeguesScreenNavigationProp;
};

const Colors = Theme.Colors;

const Leagues = ({route, navigation}: LeguesPropsType) => {
  const dispatch = useDispatch();

  // const a = useRef<FlatList>(null);

  const leguesList = useSelector(legues);
  const {loading, errors} = useSelector(leguesInfo);

  useEffect(() => {
    dispatch(getLegues());
  }, [dispatch]);

  const onSelectLegue = (item: ILegue) => {
    dispatch(selectLegue(item));
    navigation.navigate(RouteNames.teams);
  };

  const renderItem = ({item, index}: ListRenderItemInfo<ILegue>) => {
    return <LegueItem item={item} onPress={onSelectLegue} index={index} />;
  };

  return (
    <Container>
      {loading && <Loading />}

      <FlatList
        contentContainerStyle={{paddingTop: 15}}
        data={leguesList}
        keyExtractor={item => `legue_${item.leagueid}`}
        renderItem={renderItem}
      />
    </Container>
  );
};

const Container = styled.View`
  flex: 1;
  background-color: ${Colors.white};
`;

export default Leagues;
