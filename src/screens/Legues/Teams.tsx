import React, {useEffect} from 'react';
import {FlatList, ListRenderItemInfo} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';

import styled from 'styled-components/native';

import TeamItem from '@components/legues/TeamItem';
import Loading from '@components/ui/Loading';
import {
  LeguesScreenNavigationProp,
  LeguesScreenRouteProp,
} from '@navigation/RootNavigator';
import {getTeamsByLegue} from '@store/legues/actions';
import {leguesInfo, selectLegue, teamsByLegue} from '@store/legues/selectors';
import {ITeamByLegue} from '@store/legues/types';
import Theme from '@styles/theme';

type LeguesPropsType = {
  route: LeguesScreenRouteProp;
  navigation: LeguesScreenNavigationProp;
};

const Colors = Theme.Colors;

const Teams = ({route, navigation}: LeguesPropsType) => {
  const dispatch = useDispatch();

  const legue = useSelector(selectLegue);
  const teamsList = useSelector(teamsByLegue);
  const {loading, errors} = useSelector(leguesInfo);

  useEffect(() => {
    if (legue?.name) {
      navigation.setOptions({title: legue.name});
    }
  }, [legue?.name, navigation]);

  useEffect(() => {
    if (legue?.leagueid) {
      dispatch(getTeamsByLegue(legue.leagueid));
    }
  }, [dispatch, legue?.leagueid]);

  const renderItem = ({item, index}: ListRenderItemInfo<ITeamByLegue>) => {
    return <TeamItem item={item} index={index} />;
  };

  return (
    <Container>
      {loading && <Loading />}
      <FlatList
        contentContainerStyle={{paddingTop: 15}}
        data={teamsList}
        keyExtractor={item => `team_${item.team_id}`}
        renderItem={renderItem}
      />
    </Container>
  );
};

const Container = styled.View`
  flex: 1;
  background-color: ${Colors.white};
`;

export default Teams;
