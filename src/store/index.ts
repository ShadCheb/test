import {combineReducers} from 'redux';

import {heroesReducer} from './heroes/reducer';
import {leguesReducer} from './legues/reducer';
import {playersReducer} from './players/reducer';

export const rootReducer = combineReducers({
  heroes: heroesReducer,
  players: playersReducer,
  legues: leguesReducer,
});

export type AppStateType = ReturnType<typeof rootReducer>;
