import {Dispatch} from 'redux';
import {action} from 'typesafe-actions';

import {API_ENDPOINT} from '@constants/endpoints';
import {callApi} from '@utils/api';

import {BaseThunkType} from '../types';
import {HeroesActionTypes, IHero} from './types';

export const fetchHeroesRequest = () =>
  action(HeroesActionTypes.FETCH_HEROES_REQUEST);
export const fetchHeroesSuccess = (data: IHero[]) =>
  action(HeroesActionTypes.FETCH_HEROES_SUCCESS, data);
export const fetchHeroesError = (message: string) =>
  action(HeroesActionTypes.FETCH_HEROES_ERROR, message);

// Thunks
export const getHeroes = (): ThunkType => {
  return async (dispatch: Dispatch<ActionsTypes>) => {
    try {
      const res = await callApi('get', API_ENDPOINT, '/heroStats');

      if (res.error) {
        dispatch(fetchHeroesError(res.stack));
      } else {
        setTimeout(() => {
          dispatch(fetchHeroesSuccess(res));
        }, 2500);
      }
    } catch (err) {
      if (err instanceof Error && err.stack) {
        dispatch(fetchHeroesError(err.stack));
      } else {
        dispatch(fetchHeroesError('An unknown error occured.'));
      }
    }
  };
};

type ActionsTypes =
  | ReturnType<typeof fetchHeroesRequest>
  | ReturnType<typeof fetchHeroesSuccess>
  | ReturnType<typeof fetchHeroesError>;

type ThunkType = BaseThunkType<ActionsTypes>;
