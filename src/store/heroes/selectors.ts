import {AppStateType} from '@store/index';

export const heroes = (state: AppStateType) => state.heroes.data;
export const heroesInfo = (state: AppStateType) => ({
  loading: state.heroes.loading,
  errors: state.heroes.errors,
});
