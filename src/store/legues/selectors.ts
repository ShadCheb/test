import {AppStateType} from '@store/index';

export const legues = (state: AppStateType) => state.legues.data;
export const selectLegue = (state: AppStateType) => state.legues.selectLegue;
export const teamsByLegue = (state: AppStateType) => state.legues.teams;
export const leguesInfo = (state: AppStateType) => ({
  loading: state.legues.loading,
  errors: state.legues.errors,
});
