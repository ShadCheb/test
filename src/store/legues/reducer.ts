import {Reducer} from 'redux';

import {LeguesActionTypes, LeguesState} from './types';

export const initialState: LeguesState = {
  data: [],
  selectLegue: undefined,
  teams: [],
  errors: undefined,
  loading: false,
};

const reducer: Reducer<LeguesState> = (state = initialState, action) => {
  switch (action.type) {
    case LeguesActionTypes.FETCH_LEGUES_REQUEST: {
      return {...state, loading: true, errors: undefined};
    }
    case LeguesActionTypes.FETCH_LEGUES_SUCCESS: {
      return {...state, loading: false, data: action.payload};
    }
    case LeguesActionTypes.FETCH_LEGUES_ERROR: {
      return {...state, loading: false, errors: action.payload};
    }
    case LeguesActionTypes.FETCH_TEAMS_BY_LEGUE_REQUEST: {
      return {...state, loading: true, errors: undefined};
    }
    case LeguesActionTypes.FETCH_TEAMS_BY_LEGUE_SUCCESS: {
      return {...state, loading: false, teams: action.payload};
    }
    case LeguesActionTypes.FETCH_TEAMS_BY_LEGUE_ERROR: {
      return {...state, loading: false, errors: action.payload};
    }
    case LeguesActionTypes.SELECT_LEGUE: {
      return {...state, selectLegue: action.payload};
    }
    default: {
      return state;
    }
  }
};

export {reducer as leguesReducer};
