import nock from 'nock';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';

import {
  fetchLeguesError,
  fetchLeguesRequest,
  fetchLeguesSuccess,
} from '../actions';
import {getLegues} from '../actions';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

const legues = [
  {
    banner: null,
    leagueid: 11894,
    name: 'adsf',
    ticket: null,
    tier: 'premium',
  },
  {
    banner: null,
    leagueid: 10484,
    name: 'testcop',
    ticket: null,
    tier: 'excluded',
  },
];

// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
global.fetch = jest.fn(() =>
  Promise.resolve({
    json: () => legues,
  }),
);

test('test getSomeData', () => {
  const store = mockStore({});

  // TODO: Разобраться в работе nock
  // nock('https://api.opendota.com').get('/api/leagues').reply(200, legues);

  const expectedActions = [fetchLeguesRequest(), fetchLeguesSuccess(legues)];

  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  const dispatchedStore = store.dispatch(getLegues());
  return dispatchedStore.then(() => {
    expect(store.getActions()).toEqual(expectedActions);
  });
});
