import {
  fetchLeguesError,
  fetchLeguesRequest,
  fetchLeguesSuccess,
  getLegues,
} from '../actions';
import {leguesReducer} from '../reducer';
import {LeguesActionTypes, LeguesState} from '../types';

let state: LeguesState;
const legues = [
  {
    banner: null,
    leagueid: 11894,
    name: 'adsf',
    ticket: null,
    tier: 'premium',
  },
  {
    banner: null,
    leagueid: 10484,
    name: 'testcop',
    ticket: null,
    tier: 'excluded',
  },
];
const teams = [
  {
    last_match_time: 1636392036,
    logo_url:
      'https://steamcdn-a.akamaihd.net/apps/dota2/images/team_logos/46.png',
    losses: 930,
    name: 'Team Empire',
    rating: 1165.75,
    tag: 'Empire',
    team_id: 46,
    wins: 1215,
  },
  {
    last_match_time: 1624616401,
    logo_url:
      'https://steamcdn-a.akamaihd.net/apps/dota2/images/team_logos/36.png',
    losses: 848,
    name: 'Natus Vincere',
    rating: 1216.62,
    tag: 'Na`Vi',
    team_id: 36,
    wins: 1035,
  },
];

beforeEach(() => {
  state = {
    data: [],
    selectLegue: undefined,
    teams: [],
    errors: undefined,
    loading: false,
  };
});

test('fetch legues request', () => {
  const newState = leguesReducer(state, fetchLeguesRequest());

  expect(newState.loading).toBeTruthy();
});

test('fetch legues error', () => {
  const errorMessage = 'An unknown error occured.';
  const newState = leguesReducer(state, fetchLeguesError(errorMessage));

  expect(newState.errors).toBe(errorMessage);
});

// Thunks
test('fetch legues success', () => {
  const newState = leguesReducer(state, fetchLeguesSuccess(legues));
  const legue1 = newState.data[0];
  const legue2 = newState.data[1];

  expect(legue1).toEqual(legues[0]);
  expect(legue2).toEqual(legues[1]);
});
