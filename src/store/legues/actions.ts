import {Dispatch} from 'redux';
import {action} from 'typesafe-actions';

import {API_ENDPOINT} from '@constants/endpoints';
import {callApi} from '@utils/api';

import {BaseThunkType} from '../types';
import {ILegue, ITeamByLegue, LeguesActionTypes} from './types';

export const fetchLeguesRequest = () =>
  action(LeguesActionTypes.FETCH_LEGUES_REQUEST);
export const fetchLeguesSuccess = (data: ILegue[]) =>
  action(LeguesActionTypes.FETCH_LEGUES_SUCCESS, data);
export const fetchLeguesError = (message: string) =>
  action(LeguesActionTypes.FETCH_LEGUES_ERROR, message);
export const fetchTeamsByLegueRequest = () =>
  action(LeguesActionTypes.FETCH_TEAMS_BY_LEGUE_REQUEST);
export const fetchTeamsByLegueSuccess = (data: ITeamByLegue[]) =>
  action(LeguesActionTypes.FETCH_TEAMS_BY_LEGUE_SUCCESS, data);
export const fetchTeamsByLegueError = (message: string) =>
  action(LeguesActionTypes.FETCH_TEAMS_BY_LEGUE_ERROR, message);
export const selectLegue = (legue: ILegue) =>
  action(LeguesActionTypes.SELECT_LEGUE, legue);

// Thunks
export const getLegues = (): ThunkType => {
  return async (dispatch: Dispatch<ActionsTypes>) => {
    try {
      dispatch(fetchLeguesRequest());
      const res = await callApi('get', API_ENDPOINT, '/leagues');

      if (res.error) {
        dispatch(fetchLeguesError(res.stack));
        return;
      }
      // setTimeout(() => {
      //   const result = res.slice(0, 20);

      //   dispatch(fetchLeguesSuccess(result));
      // }, 2500);

      const result = res.slice(0, 20);
      dispatch(fetchLeguesSuccess(result));
    } catch (err) {
      if (err instanceof Error && err.stack) {
        dispatch(fetchLeguesError(err.stack));
      } else {
        dispatch(fetchLeguesError('An unknown error occured.'));
      }
    }
  };
};

export const getTeamsByLegue = (leagueId: number): ThunkType => {
  return async (dispatch: Dispatch<ActionsTypes>) => {
    try {
      dispatch(fetchTeamsByLegueRequest());
      const res = await callApi(
        'get',
        API_ENDPOINT,
        `/leagues/${leagueId}/teams`,
      );

      if (res.error) {
        dispatch(fetchTeamsByLegueError(res.stack));
        return;
      }
      setTimeout(() => {
        dispatch(fetchTeamsByLegueSuccess(res));
      }, 2500);
    } catch (err) {
      if (err instanceof Error && err.stack) {
        dispatch(fetchTeamsByLegueError(err.stack));
      } else {
        dispatch(fetchTeamsByLegueError('An unknown error occured.'));
      }
    }
  };
};

type ActionsTypes =
  | ReturnType<typeof fetchLeguesRequest>
  | ReturnType<typeof fetchLeguesSuccess>
  | ReturnType<typeof fetchLeguesError>
  | ReturnType<typeof fetchTeamsByLegueRequest>
  | ReturnType<typeof fetchTeamsByLegueSuccess>
  | ReturnType<typeof fetchTeamsByLegueError>
  | ReturnType<typeof selectLegue>;

type ThunkType = BaseThunkType<ActionsTypes>;
