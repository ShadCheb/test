export interface ILegue extends ApiResponse {
  leagueid: number;
  ticket: string | null;
  banner: string | null;
  tier: string;
  name: string;
}

export interface ITeamByLegue extends ApiResponse {
  team_id: number;
  rating: number;
  wins: number;
  losses: number;
  last_match_time: number;
  name: string;
  tag: string;
}

export type ApiResponse = Record<string, any>;

export enum LeguesActionTypes {
  FETCH_LEGUES_REQUEST = 'legues/FETCH_REQUEST',
  FETCH_LEGUES_SUCCESS = 'legues/FETCH_SUCCESS',
  FETCH_LEGUES_ERROR = 'legues/FETCH_ERROR',
  FETCH_TEAMS_BY_LEGUE_REQUEST = 'legues/teams/FETCH_REQUEST',
  FETCH_TEAMS_BY_LEGUE_SUCCESS = 'legues/teams/FETCH_SUCCESS',
  FETCH_TEAMS_BY_LEGUE_ERROR = 'legues/teams/FETCH_ERROR',
  SELECT_LEGUE = 'legues/teams/SELECT_LEGUE',
}

export interface LeguesState {
  readonly loading: boolean;
  readonly data: ILegue[];
  readonly teams: ITeamByLegue[];
  readonly selectLegue?: ILegue;
  readonly errors?: string;
}
