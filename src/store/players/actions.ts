import {Dispatch} from 'redux';
import {action} from 'typesafe-actions';

import {API_ENDPOINT} from '@constants/endpoints';
import {callApi} from '@utils/api';

import {BaseThunkType} from '../types';
import {Player, PlayersActionTypes} from './types';

export const fetchPlayersRequest = () =>
  action(PlayersActionTypes.FETCH_PLAYERS_REQUEST);
export const fetchPlayersSuccess = (data: Player[]) =>
  action(PlayersActionTypes.FETCH_PLAYERS_SUCCESS, data);
export const fetchPlayersError = (message: string) =>
  action(PlayersActionTypes.FETCH_PLAYERS_ERROR, message);

// Thunks
export const getHeroes = (): ThunkType => {
  return async (dispatch: Dispatch<ActionsTypes>) => {
    try {
      const res = await callApi('get', API_ENDPOINT, '/proPlayers');

      if (res.error) {
        dispatch(fetchPlayersError(res.stack));
      } else {
        setTimeout(() => {
          dispatch(fetchPlayersSuccess(res));
        }, 2500);
      }
    } catch (err) {
      if (err instanceof Error && err.stack) {
        dispatch(fetchPlayersError(err.stack));
      } else {
        dispatch(fetchPlayersError('An unknown error occured.'));
      }
    }
  };
};

type ActionsTypes =
  | ReturnType<typeof fetchPlayersRequest>
  | ReturnType<typeof fetchPlayersSuccess>
  | ReturnType<typeof fetchPlayersError>;

type ThunkType = BaseThunkType<ActionsTypes>;
