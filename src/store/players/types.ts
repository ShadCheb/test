export interface Player extends ApiResponse {
  account_id: number;
  steamid: string;
  avatar: string;
  avatarmedium: string;
  avatarfull: string;
  profileurl: string;
  personaname: string;
  last_login: string;
  full_history_time: string;
  cheese: number;
  fh_unavailable: boolean;
  loccountrycode: string;
  name: string;
  country_code: string;
  fantasy_role: number;
  team_id: number;
  team_name: string;
  team_tag: string;
  is_locked: boolean;
  is_pro: boolean;
  locked_until: number;
}

export type ApiResponse = Record<string, any>;

export enum PlayersActionTypes {
  FETCH_PLAYERS_REQUEST = 'players/FETCH_REQUEST',
  FETCH_PLAYERS_SUCCESS = 'players/FETCH_SUCCESS',
  FETCH_PLAYERS_ERROR = 'players/FETCH_ERROR',
  SELECT_PLAYER = 'players/SELECT_PLAYER',
  SELECTED_PLAYERS = 'players/SELECTED',
}

export interface PlayersState {
  readonly loading: boolean;
  readonly data: Player[];
  readonly errors?: string;
}
