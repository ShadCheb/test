import {Reducer} from 'redux';

import {PlayersActionTypes, PlayersState} from './types';

export const initialState: PlayersState = {
  data: [],
  errors: undefined,
  loading: false,
};

const reducer: Reducer<PlayersState> = (state = initialState, action) => {
  switch (action.type) {
    case PlayersActionTypes.FETCH_PLAYERS_REQUEST: {
      return {...state, loading: true};
    }
    case PlayersActionTypes.FETCH_PLAYERS_SUCCESS: {
      return {...state, loading: false, data: action.payload};
    }
    case PlayersActionTypes.FETCH_PLAYERS_ERROR: {
      return {...state, loading: false, errors: action.payload};
    }
    default: {
      return state;
    }
  }
};

export {reducer as playersReducer};
