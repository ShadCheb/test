import {Action} from 'redux';
import {ThunkAction} from 'redux-thunk';

import {AppStateType} from './';

export type InferActionsTypes<T> = T extends {
  [keys: string]: (...args: any[]) => infer U;
}
  ? U
  : never;

export type BaseThunkType<
  A extends Action = Action,
  R = Promise<void>,
> = ThunkAction<R, AppStateType, unknown, A>;
