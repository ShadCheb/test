const config = {
  verbose: true,
  preset: 'react-native',
    moduleFileExtensions: [
      'ts',
      'tsx',
      'js',
      'jsx',
      'json',
      'node'
    ],
    setupFiles: ['./__mocks__/jest-setup.js'],
    transformIgnorePatterns: ['node_modules/(?!@react-native|react-native)']
};

module.exports = config;
