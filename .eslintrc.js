module.exports = {
  root: true,
  parserOptions: {
    project: './jsconfig.json',
  },
  // parser: 'babel-eslint',
  parser: '@typescript-eslint/parser', // Задает parser ESLint
  extends: [
    '@react-native-community',
    'prettier',
    'plugin:@typescript-eslint/recommended',
  ],
  plugins: ['simple-import-sort', 'react-hooks', 'jest'],
  env: {
    'jest/globals': true
  },
  rules: {
    'no-console': 'error',
    'prefer-template': 'error',
    'react/jsx-boolean-value': 'warn',
    'react/jsx-curly-brace-presence': ['warn', 'never'],
    'react-hooks/rules-of-hooks': 'error',
    'react-hooks/exhaustive-deps': 'error',
    // sort
    'sort-imports': 'off',
    'import/order': 'off',
    'simple-import-sort/imports': [
      'error',
      {
        groups: [
          ['^\\u0000'], // bare imports
          ['^react'], // react
          ['^[^\\.]'], // non-local imports
          [
            '^@navigation|^@screens|^@services|^@components|^@store|^@hooks|^@utils|^@styles|^@constants|^@assets',
          ], // internal
          ['^\\.'], // local imports
        ],
      },
    ],
    'jest/no-disabled-tests': 'warn',
    'jest/no-focused-tests': 'error',
    'jest/no-identical-title': 'error',
    'jest/prefer-to-have-length': 'warn',
    'jest/valid-expect': 'error',
    '@typescript-eslint/ban-types': [
      'error',
      {
        extendDefaults: true,
        types: {
          '{}': false
        }
      }
    ]
  },
  ignorePatterns: ["/*.*"],
};
