module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    ["transform-remove-console", { "exclude": ["error", "log"] }],
    [
      'module-resolver',
      {
        root: ['./src'],
        extensions: ['.ios.js', '.android.js', '.js', '.ts', '.tsx', '.json'],
        alias: {
          tests: './__tests__',
          '@navigation': './src/navigation',
          '@screens': './src/screens',
          '@components': './src/components',
          '@store': './src/store',
          '@hooks': './src/hooks',
          '@utils': './src/utils',
          '@services': './src/services',
          '@styles': './src/styles',
          '@assets': './src/assets',
          '@constants': './src/constants',
        },
      },
    ],
    'react-native-reanimated/plugin',
  ],
};
